/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;

/**
 *
 * @author jmejia
 */
public class Hl7Message implements Serializable{
    private PatientMsh patientMsh;
    private PatientFt1 patientFt1;
    private PatientNte patientNte;
    private PatientOrc patientOrc;
    private PatientPid patientPid;
    private PatientRxc patientRxc;
    private PatientRxd patientRxd;
    private PatientRxe patientRxe;
    private PatientRxr patientRxr;
    private PatientTq1 patientTq1;
    private PatientVisit patientVisit;
    private AcuseMsa acuseMsa;

    public Hl7Message(){
        
    }
    
    /**
     * @return the patientOrc
     */
    public PatientOrc getPatientOrc() {
        return patientOrc;
    }

    /**
     * @param patientOrc the patientOrc to set
     */
    public void setPatientOrc(PatientOrc patientOrc) {
        this.patientOrc = patientOrc;
    }

    /**
     * @return the patientPid
     */
    public PatientPid getPatientPid() {
        return patientPid;
    }

    /**
     * @param patientPid the patientPid to set
     */
    public void setPatientPid(PatientPid patientPid) {
        this.patientPid = patientPid;
    }

    /**
     * @return the patientRxc
     */
    public PatientRxc getPatientRxc() {
        return patientRxc;
    }

    /**
     * @param patientRxc the patientRxc to set
     */
    public void setPatientRxc(PatientRxc patientRxc) {
        this.patientRxc = patientRxc;
    }

    /**
     * @return the patientRxd
     */
    public PatientRxd getPatientRxd() {
        return patientRxd;
    }

    /**
     * @param patientRxd the patientRxd to set
     */
    public void setPatientRxd(PatientRxd patientRxd) {
        this.patientRxd = patientRxd;
    }

    /**
     * @return the patientRxe
     */
    public PatientRxe getPatientRxe() {
        return patientRxe;
    }

    /**
     * @param patientRxe the patientRxe to set
     */
    public void setPatientRxe(PatientRxe patientRxe) {
        this.patientRxe = patientRxe;
    }

    /**
     * @return the patientRxr
     */
    public PatientRxr getPatientRxr() {
        return patientRxr;
    }

    /**
     * @param patientRxr the patientRxr to set
     */
    public void setPatientRxr(PatientRxr patientRxr) {
        this.patientRxr = patientRxr;
    }

    /**
     * @return the patientTq1
     */
    public PatientTq1 getPatientTq1() {
        return patientTq1;
    }

    /**
     * @param patientTq1 the patientTq1 to set
     */
    public void setPatientTq1(PatientTq1 patientTq1) {
        this.patientTq1 = patientTq1;
    }

    /**
     * @return the patientVisit
     */
    public PatientVisit getPatientVisit() {
        return patientVisit;
    }

    /**
     * @param patientVisit the patientVisit to set
     */
    public void setPatientVisit(PatientVisit patientVisit) {
        this.patientVisit = patientVisit;
    }

    /**
     * @return the patientFt1
     */
    public PatientFt1 getPatientFt1() {
        return patientFt1;
    }

    /**
     * @param patientFt1 the patientFt1 to set
     */
    public void setPatientFt1(PatientFt1 patientFt1) {
        this.patientFt1 = patientFt1;
    }

    /**
     * @return the patientNte
     */
    public PatientNte getPatientNte() {
        return patientNte;
    }

    /**
     * @param patientNte the patientNte to set
     */
    public void setPatientNte(PatientNte patientNte) {
        this.patientNte = patientNte;
    }

    /**
     * @return the acuseMsa
     */
    public AcuseMsa getAcuseMsa() {
        return acuseMsa;
    }

    /**
     * @param acuseMsa the acuseMsa to set
     */
    public void setAcuseMsa(AcuseMsa acuseMsa) {
        this.acuseMsa = acuseMsa;
    }

    /**
     * @return the patientMsh
     */
    public PatientMsh getPatientMsh() {
        return patientMsh;
    }

    /**
     * @param patientMsh the patientMsh to set
     */
    public void setPatientMsh(PatientMsh patientMsh) {
        this.patientMsh = patientMsh;
    }
}
