/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;

/**
 *
 * @author jmejia
 */
public class PatientNte implements Serializable{
    private String idSegmentoNte;
    private String comment;

    /**
     * @return the idSegmentoNte
     */
    public String getIdSegmentoNte() {
        return idSegmentoNte;
    }

    /**
     * @param idSegmentoNte the idSegmentoNte to set
     */
    public void setIdSegmentoNte(String idSegmentoNte) {
        this.idSegmentoNte = idSegmentoNte;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }
    
}
