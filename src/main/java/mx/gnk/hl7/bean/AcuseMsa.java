/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;

/**
 *
 * @author jmejia
 */
public class AcuseMsa implements Serializable{
    private String idAcuse;
    private String idControlSolicitud;
    private String descCondError;

    /**
     * @return the idAcuse
     */
    public String getIdAcuse() {
        return idAcuse;
    }

    /**
     * @param idAcuse the idAcuse to set
     */
    public void setIdAcuse(String idAcuse) {
        this.idAcuse = idAcuse;
    }

    /**
     * @return the idControlSolicitud
     */
    public String getIdControlSolicitud() {
        return idControlSolicitud;
    }

    /**
     * @param idControlSolicitud the idControlSolicitud to set
     */
    public void setIdControlSolicitud(String idControlSolicitud) {
        this.idControlSolicitud = idControlSolicitud;
    }

    /**
     * @return the descCondError
     */
    public String getDescCondError() {
        return descCondError;
    }

    /**
     * @param descCondError the descCondError to set
     */
    public void setDescCondError(String descCondError) {
        this.descCondError = descCondError;
    }
}
