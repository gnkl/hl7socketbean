/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;

/**
 *
 * @author jmejia
 */
public class PatientOrc implements Serializable{
    private String orderControl;
    private String idReceta;
    private String idUnidadMedica;
    private String idSolicitud;
    private String idUnidadMedicaGroup;
    private String fecha;
    
    //RDEO11
    private String sistemaId;
    private String proveedorOrden;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String primerNombre;
    private String segundoNombre;
    
    //
    private String groupNumberId;
    

    /**
     * @return the orderControl
     */
    public String getOrderControl() {
        return orderControl;
    }

    /**
     * @param orderControl the orderControl to set
     */
    public void setOrderControl(String orderControl) {
        this.orderControl = orderControl;
    }

    /**
     * @return the idReceta
     */
    public String getIdReceta() {
        return idReceta;
    }

    /**
     * @param idReceta the idReceta to set
     */
    public void setIdReceta(String idReceta) {
        this.idReceta = idReceta;
    }

    /**
     * @return the idUnidadMedica
     */
    public String getIdUnidadMedica() {
        return idUnidadMedica;
    }

    /**
     * @param idUnidadMedica the idUnidadMedica to set
     */
    public void setIdUnidadMedica(String idUnidadMedica) {
        this.idUnidadMedica = idUnidadMedica;
    }

    /**
     * @return the idSolicitud
     */
    public String getIdSolicitud() {
        return idSolicitud;
    }

    /**
     * @param idSolicitud the idSolicitud to set
     */
    public void setIdSolicitud(String idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    /**
     * @return the idUnidadMedicaGroup
     */
    public String getIdUnidadMedicaGroup() {
        return idUnidadMedicaGroup;
    }

    /**
     * @param idUnidadMedicaGroup the idUnidadMedicaGroup to set
     */
    public void setIdUnidadMedicaGroup(String idUnidadMedicaGroup) {
        this.idUnidadMedicaGroup = idUnidadMedicaGroup;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the sistemaId
     */
    public String getSistemaId() {
        return sistemaId;
    }

    /**
     * @param sistemaId the sistemaId to set
     */
    public void setSistemaId(String sistemaId) {
        this.sistemaId = sistemaId;
    }

    /**
     * @return the proveedorOrden
     */
    public String getProveedorOrden() {
        return proveedorOrden;
    }

    /**
     * @param proveedorOrden the proveedorOrden to set
     */
    public void setProveedorOrden(String proveedorOrden) {
        this.proveedorOrden = proveedorOrden;
    }

    /**
     * @return the apellidoPaterno
     */
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    /**
     * @param apellidoPaterno the apellidoPaterno to set
     */
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    /**
     * @return the apellidoMaterno
     */
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    /**
     * @param apellidoMaterno the apellidoMaterno to set
     */
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    /**
     * @return the primerNombre
     */
    public String getPrimerNombre() {
        return primerNombre;
    }

    /**
     * @param primerNombre the primerNombre to set
     */
    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    /**
     * @return the segundoNombre
     */
    public String getSegundoNombre() {
        return segundoNombre;
    }

    /**
     * @param segundoNombre the segundoNombre to set
     */
    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    /**
     * @return the groupNumberId
     */
    public String getGroupNumberId() {
        return groupNumberId;
    }

    /**
     * @param groupNumberId the groupNumberId to set
     */
    public void setGroupNumberId(String groupNumberId) {
        this.groupNumberId = groupNumberId;
    }
}
