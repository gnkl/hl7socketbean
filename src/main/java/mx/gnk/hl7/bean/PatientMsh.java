/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;

/**
 *
 * @author jmejia
 */
public class PatientMsh implements Serializable{
    private String fieldSeparator;
    private String characterEncoding;
    private String sistemaEnvio;
    private String idUnidadMedica;
    private String sistemaRecibe;
    private String idUnidadMedica1;
    private String fecha;
    private String codigoMensaje;
    private String eventoMensaje;
    private String estructuraMensaje;
    private String idHl7Mensaje;
    private String idProceso;
    private String idVersion;
    
    // RRE_O12
    private String acceptAckType;
    private String appAckType;
    


    /**
     * @return the characterEncoding
     */
    public String getCharacterEncoding() {
        return characterEncoding;
    }

    /**
     * @param characterEncoding the characterEncoding to set
     */
    public void setCharacterEncoding(String characterEncoding) {
        this.characterEncoding = characterEncoding;
    }

    /**
     * @return the sistemaEnvio
     */
    public String getSistemaEnvio() {
        return sistemaEnvio;
    }

    /**
     * @param sistemaEnvio the sistemaEnvio to set
     */
    public void setSistemaEnvio(String sistemaEnvio) {
        this.sistemaEnvio = sistemaEnvio;
    }

    /**
     * @return the idUnidadMedica
     */
    public String getIdUnidadMedica() {
        return idUnidadMedica;
    }

    /**
     * @param idUnidadMedica the idUnidadMedica to set
     */
    public void setIdUnidadMedica(String idUnidadMedica) {
        this.idUnidadMedica = idUnidadMedica;
    }

    /**
     * @return the sistemaRecibe
     */
    public String getSistemaRecibe() {
        return sistemaRecibe;
    }

    /**
     * @param sistemaRecibe the sistemaRecibe to set
     */
    public void setSistemaRecibe(String sistemaRecibe) {
        this.sistemaRecibe = sistemaRecibe;
    }

    /**
     * @return the idUnidadMedica1
     */
    public String getIdUnidadMedica1() {
        return idUnidadMedica1;
    }

    /**
     * @param idUnidadMedica1 the idUnidadMedica1 to set
     */
    public void setIdUnidadMedica1(String idUnidadMedica1) {
        this.idUnidadMedica1 = idUnidadMedica1;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the codigoMensaje
     */
    public String getCodigoMensaje() {
        return codigoMensaje;
    }

    /**
     * @param codigoMensaje the codigoMensaje to set
     */
    public void setCodigoMensaje(String codigoMensaje) {
        this.codigoMensaje = codigoMensaje;
    }

    /**
     * @return the eventoMensaje
     */
    public String getEventoMensaje() {
        return eventoMensaje;
    }

    /**
     * @param eventoMensaje the eventoMensaje to set
     */
    public void setEventoMensaje(String eventoMensaje) {
        this.eventoMensaje = eventoMensaje;
    }

    /**
     * @return the estructuraMensaje
     */
    public String getEstructuraMensaje() {
        return estructuraMensaje;
    }

    /**
     * @param estructuraMensaje the estructuraMensaje to set
     */
    public void setEstructuraMensaje(String estructuraMensaje) {
        this.estructuraMensaje = estructuraMensaje;
    }

    /**
     * @return the idHl7Mensaje
     */
    public String getIdHl7Mensaje() {
        return idHl7Mensaje;
    }

    /**
     * @param idHl7Mensaje the idHl7Mensaje to set
     */
    public void setIdHl7Mensaje(String idHl7Mensaje) {
        this.idHl7Mensaje = idHl7Mensaje;
    }

    /**
     * @return the idProceso
     */
    public String getIdProceso() {
        return idProceso;
    }

    /**
     * @param idProceso the idProceso to set
     */
    public void setIdProceso(String idProceso) {
        this.idProceso = idProceso;
    }

    /**
     * @return the idVersion
     */
    public String getIdVersion() {
        return idVersion;
    }

    /**
     * @param idVersion the idVersion to set
     */
    public void setIdVersion(String idVersion) {
        this.idVersion = idVersion;
    }

    /**
     * @return the fieldSeparator
     */
    public String getFieldSeparator() {
        return fieldSeparator;
    }

    /**
     * @param fieldSeparator the fieldSeparator to set
     */
    public void setFieldSeparator(String fieldSeparator) {
        this.fieldSeparator = fieldSeparator;
    }

    /**
     * @return the acceptAckType
     */
    public String getAcceptAckType() {
        return acceptAckType;
    }

    /**
     * @param acceptAckType the acceptAckType to set
     */
    public void setAcceptAckType(String acceptAckType) {
        this.acceptAckType = acceptAckType;
    }

    /**
     * @return the appAckType
     */
    public String getAppAckType() {
        return appAckType;
    }

    /**
     * @param appAckType the appAckType to set
     */
    public void setAppAckType(String appAckType) {
        this.appAckType = appAckType;
    }
}
