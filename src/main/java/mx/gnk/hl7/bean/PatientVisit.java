/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;

/**
 *
 * @author jmejia
 */
public class PatientVisit implements Serializable{
    private Long idSegmento;
    private String viaAdmon;
    private Long puntoAtencion;
    private Long cuarto;
    private String cama;
    private String unidadMedica;
    private Long numeroVisita;
    private String fechaSolicitud;
    
    // TODO AGREGAR MAS COSAS
    private String idPersona;
    private String tipoCodigo;
    private String idUnidadMedica;
    

    /**
     * @return the idSegmento
     */
    public Long getIdSegmento() {
        return idSegmento;
    }

    /**
     * @param idSegmento the idSegmento to set
     */
    public void setIdSegmento(Long idSegmento) {
        this.idSegmento = idSegmento;
    }

    /**
     * @return the viaAdmon
     */
    public String getViaAdmon() {
        return viaAdmon;
    }

    /**
     * @param viaAdmon the viaAdmon to set
     */
    public void setViaAdmon(String viaAdmon) {
        this.viaAdmon = viaAdmon;
    }

    /**
     * @return the puntoAtencion
     */
    public Long getPuntoAtencion() {
        return puntoAtencion;
    }

    /**
     * @param puntoAtencion the puntoAtencion to set
     */
    public void setPuntoAtencion(Long puntoAtencion) {
        this.puntoAtencion = puntoAtencion;
    }

    /**
     * @return the cuarto
     */
    public Long getCuarto() {
        return cuarto;
    }

    /**
     * @param cuarto the cuarto to set
     */
    public void setCuarto(Long cuarto) {
        this.cuarto = cuarto;
    }

    /**
     * @return the cama
     */
    public String getCama() {
        return cama;
    }

    /**
     * @param cama the cama to set
     */
    public void setCama(String cama) {
        this.cama = cama;
    }

    /**
     * @return the unidadMedica
     */
    public String getUnidadMedica() {
        return unidadMedica;
    }

    /**
     * @param unidadMedica the unidadMedica to set
     */
    public void setUnidadMedica(String unidadMedica) {
        this.unidadMedica = unidadMedica;
    }

    /**
     * @return the numeroVisita
     */
    public Long getNumeroVisita() {
        return numeroVisita;
    }

    /**
     * @param numeroVisita the numeroVisita to set
     */
    public void setNumeroVisita(Long numeroVisita) {
        this.numeroVisita = numeroVisita;
    }

    /**
     * @return the fechaSolicitud
     */
    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    /**
     * @param fechaSolicitud the fechaSolicitud to set
     */
    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    /**
     * @return the idPersona
     */
    public String getIdPersona() {
        return idPersona;
    }

    /**
     * @param idPersona the idPersona to set
     */
    public void setIdPersona(String idPersona) {
        this.idPersona = idPersona;
    }

    /**
     * @return the tipoCodigo
     */
    public String getTipoCodigo() {
        return tipoCodigo;
    }

    /**
     * @param tipoCodigo the tipoCodigo to set
     */
    public void setTipoCodigo(String tipoCodigo) {
        this.tipoCodigo = tipoCodigo;
    }

    /**
     * @return the idUnidadMedica
     */
    public String getIdUnidadMedica() {
        return idUnidadMedica;
    }

    /**
     * @param idUnidadMedica the idUnidadMedica to set
     */
    public void setIdUnidadMedica(String idUnidadMedica) {
        this.idUnidadMedica = idUnidadMedica;
    }
    
    
}
