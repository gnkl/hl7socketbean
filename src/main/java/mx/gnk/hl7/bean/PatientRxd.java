/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;

/**
 *
 * @author jmejia
 */
public class PatientRxd implements Serializable{
    private Long contador;
    private String codigoMedicamento;
    private String descripcionMedicamento;
    private String fechaEntrega;
    private Long cantidadMedicamento;
    private String presentacionMedicamento;
    private String numeroPrescripcion;
    private String tipoEntrega;

    /**
     * @return the contador
     */
    public Long getContador() {
        return contador;
    }

    /**
     * @param contador the contador to set
     */
    public void setContador(Long contador) {
        this.contador = contador;
    }

    /**
     * @return the codigoMedicamento
     */
    public String getCodigoMedicamento() {
        return codigoMedicamento;
    }

    /**
     * @param codigoMedicamento the codigoMedicamento to set
     */
    public void setCodigoMedicamento(String codigoMedicamento) {
        this.codigoMedicamento = codigoMedicamento;
    }

    /**
     * @return the descripcionMedicamento
     */
    public String getDescripcionMedicamento() {
        return descripcionMedicamento;
    }

    /**
     * @param descripcionMedicamento the descripcionMedicamento to set
     */
    public void setDescripcionMedicamento(String descripcionMedicamento) {
        this.descripcionMedicamento = descripcionMedicamento;
    }

    /**
     * @return the fechaEntrega
     */
    public String getFechaEntrega() {
        return fechaEntrega;
    }

    /**
     * @param fechaEntrega the fechaEntrega to set
     */
    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    /**
     * @return the cantidadMedicamento
     */
    public Long getCantidadMedicamento() {
        return cantidadMedicamento;
    }

    /**
     * @param cantidadMedicamento the cantidadMedicamento to set
     */
    public void setCantidadMedicamento(Long cantidadMedicamento) {
        this.cantidadMedicamento = cantidadMedicamento;
    }

    /**
     * @return the presentacionMedicamento
     */
    public String getPresentacionMedicamento() {
        return presentacionMedicamento;
    }

    /**
     * @param presentacionMedicamento the presentacionMedicamento to set
     */
    public void setPresentacionMedicamento(String presentacionMedicamento) {
        this.presentacionMedicamento = presentacionMedicamento;
    }

    /**
     * @return the numeroPrescripcion
     */
    public String getNumeroPrescripcion() {
        return numeroPrescripcion;
    }

    /**
     * @param numeroPrescripcion the numeroPrescripcion to set
     */
    public void setNumeroPrescripcion(String numeroPrescripcion) {
        this.numeroPrescripcion = numeroPrescripcion;
    }

    /**
     * @return the tipoEntrega
     */
    public String getTipoEntrega() {
        return tipoEntrega;
    }

    /**
     * @param tipoEntrega the tipoEntrega to set
     */
    public void setTipoEntrega(String tipoEntrega) {
        this.tipoEntrega = tipoEntrega;
    }
    
}
