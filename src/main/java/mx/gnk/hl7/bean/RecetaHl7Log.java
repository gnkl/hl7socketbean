/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author jmejia
 */
public class RecetaHl7Log implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String tipo_mensaje;
    private String hl7_mensaje;
    private Date fecha;
    private Integer estatus;
    private String proceso;

    public RecetaHl7Log() {
    }

    public RecetaHl7Log(Long id) {
        this.id = id;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RecetaHl7Log)) {
            return false;
        }
        RecetaHl7Log other = (RecetaHl7Log) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gnk.hl7.bean.RecetaHl7Log[ id=" + getId() + " ]";
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the tipo_mensaje
     */
    public String getTipo_mensaje() {
        return tipo_mensaje;
    }

    /**
     * @param tipo_mensaje the tipo_mensaje to set
     */
    public void setTipo_mensaje(String tipo_mensaje) {
        this.tipo_mensaje = tipo_mensaje;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the estatus
     */
    public Integer getEstatus() {
        return estatus;
    }

    /**
     * @param estatus the estatus to set
     */
    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    /**
     * @return the proceso
     */
    public String getProceso() {
        return proceso;
    }

    /**
     * @param proceso the proceso to set
     */
    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    /**
     * @return the hl7_mensaje
     */
    public String getHl7_mensaje() {
        return hl7_mensaje;
    }

    /**
     * @param hl7_mensaje the hl7_mensaje to set
     */
    public void setHl7_mensaje(String hl7_mensaje) {
        this.hl7_mensaje = hl7_mensaje;
    }
    
}
