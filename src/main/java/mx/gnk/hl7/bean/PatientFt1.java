/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;

/**
 *
 * @author jmejia
 */
public class PatientFt1 implements Serializable{
    private String idSegmentoFt1;
    private String fechaSolicitud;
    private String tipoTransaccion;
    private String idCodigoTransaccion;
    private String codigoDiagnostico;
    private String nombreCodigoDiagnostico;    

    /**
     * @return the idSegmentoFt1
     */
    public String getIdSegmentoFt1() {
        return idSegmentoFt1;
    }

    /**
     * @param idSegmentoFt1 the idSegmentoFt1 to set
     */
    public void setIdSegmentoFt1(String idSegmentoFt1) {
        this.idSegmentoFt1 = idSegmentoFt1;
    }

    /**
     * @return the fechaSolicitud
     */
    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    /**
     * @param fechaSolicitud the fechaSolicitud to set
     */
    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    /**
     * @return the tipoTransaccion
     */
    public String getTipoTransaccion() {
        return tipoTransaccion;
    }

    /**
     * @param tipoTransaccion the tipoTransaccion to set
     */
    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    /**
     * @return the idCodigoTransaccion
     */
    public String getIdCodigoTransaccion() {
        return idCodigoTransaccion;
    }

    /**
     * @param idCodigoTransaccion the idCodigoTransaccion to set
     */
    public void setIdCodigoTransaccion(String idCodigoTransaccion) {
        this.idCodigoTransaccion = idCodigoTransaccion;
    }

    /**
     * @return the codigoDiagnostico
     */
    public String getCodigoDiagnostico() {
        return codigoDiagnostico;
    }

    /**
     * @param codigoDiagnostico the codigoDiagnostico to set
     */
    public void setCodigoDiagnostico(String codigoDiagnostico) {
        this.codigoDiagnostico = codigoDiagnostico;
    }

    /**
     * @return the nombreCodigoDiagnostico
     */
    public String getNombreCodigoDiagnostico() {
        return nombreCodigoDiagnostico;
    }

    /**
     * @param nombreCodigoDiagnostico the nombreCodigoDiagnostico to set
     */
    public void setNombreCodigoDiagnostico(String nombreCodigoDiagnostico) {
        this.nombreCodigoDiagnostico = nombreCodigoDiagnostico;
    }
    
}
