/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;

/**
 *
 * @author jmejia
 */
public class PatientPid implements Serializable{
    private Long idSegmento;
    private Long idPaciente;
    private String assignAutoridad;
    private String tipoCodigo;
    private String curp;
    private String jurisdiccion;
    private String agenciaDepto;
    private String apPaterno;
    private String apMaterno;
    private String primerNombre;
    private String segundoNombre;
    private String fechaNacimiento;
    private String sexo;
    
    public PatientPid(){
    }
    
    /**
     * @return the idSegmento
     */
    public Long getIdSegmento() {
        return idSegmento;
    }

    /**
     * @param idSegmento the idSegmento to set
     */
    public void setIdSegmento(Long idSegmento) {
        this.idSegmento = idSegmento;
    }

    /**
     * @return the idPaciente
     */
    public Long getIdPaciente() {
        return idPaciente;
    }

    /**
     * @param idPaciente the idPaciente to set
     */
    public void setIdPaciente(Long idPaciente) {
        this.idPaciente = idPaciente;
    }

    /**
     * @return the tipoCodigo
     */
    public String getTipoCodigo() {
        return tipoCodigo;
    }

    /**
     * @param tipoCodigo the tipoCodigo to set
     */
    public void setTipoCodigo(String tipoCodigo) {
        this.tipoCodigo = tipoCodigo;
    }

    /**
     * @return the curp
     */
    public String getCurp() {
        return curp;
    }

    /**
     * @param curp the curp to set
     */
    public void setCurp(String curp) {
        this.curp = curp;
    }

    /**
     * @return the jurisdiccion
     */
    public String getJurisdiccion() {
        return jurisdiccion;
    }

    /**
     * @param jurisdiccion the jurisdiccion to set
     */
    public void setJurisdiccion(String jurisdiccion) {
        this.jurisdiccion = jurisdiccion;
    }

    /**
     * @return the agenciaDepto
     */
    public String getAgenciaDepto() {
        return agenciaDepto;
    }

    /**
     * @param agenciaDepto the agenciaDepto to set
     */
    public void setAgenciaDepto(String agenciaDepto) {
        this.agenciaDepto = agenciaDepto;
    }

    /**
     * @return the apPaterno
     */
    public String getApPaterno() {
        return apPaterno;
    }

    /**
     * @param apPaterno the apPaterno to set
     */
    public void setApPaterno(String apPaterno) {
        this.apPaterno = apPaterno;
    }

    /**
     * @return the apMaterno
     */
    public String getApMaterno() {
        return apMaterno;
    }

    /**
     * @param apMaterno the apMaterno to set
     */
    public void setApMaterno(String apMaterno) {
        this.apMaterno = apMaterno;
    }

    /**
     * @return the primerNombre
     */
    public String getPrimerNombre() {
        return primerNombre;
    }

    /**
     * @param primerNombre the primerNombre to set
     */
    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    /**
     * @return the segundoNombre
     */
    public String getSegundoNombre() {
        return segundoNombre;
    }

    /**
     * @param segundoNombre the segundoNombre to set
     */
    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    /**
     * @return the fechaNacimiento
     */
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * @param fechaNacimiento the fechaNacimiento to set
     */
    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the assignAutoridad
     */
    public String getAssignAutoridad() {
        return assignAutoridad;
    }

    /**
     * @param assignAutoridad the assignAutoridad to set
     */
    public void setAssignAutoridad(String assignAutoridad) {
        this.assignAutoridad = assignAutoridad;
    }
}
