/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;

/**
 *
 * @author jmejia
 */
public class PatientRxe implements Serializable{
    
    private Long cantidadMedicamento;
    private String intervaloRepeticion;
    private String intervaloTiempoExplicito;
    private Integer duracionTratamiento;
    private String fechaIniPrescripcion;
    private String fechaFinPrescripcion;
    private String condicionesUso;
    private String textoAdicional;
    private String idMedicamento;
    private String descMedicamento;
    private String codigoMedicamento;
    private String cantMinEntrega;
    private String cantMaxEntrega;
    private String unidadMedida;
    private String dosis;
    private String instrucciones;
    private Long cantidadAdministrar;
    private String cantidadTotal;

    /**
     * @return the cantidadMedicamento
     */
    public Long getCantidadMedicamento() {
        return cantidadMedicamento;
    }

    /**
     * @param cantidadMedicamento the cantidadMedicamento to set
     */
    public void setCantidadMedicamento(Long cantidadMedicamento) {
        this.cantidadMedicamento = cantidadMedicamento;
    }

    /**
     * @return the intervaloRepeticion
     */
    public String getIntervaloRepeticion() {
        return intervaloRepeticion;
    }

    /**
     * @param intervaloRepeticion the intervaloRepeticion to set
     */
    public void setIntervaloRepeticion(String intervaloRepeticion) {
        this.intervaloRepeticion = intervaloRepeticion;
    }

    /**
     * @return the intervaloTiempoExplicito
     */
    public String getIntervaloTiempoExplicito() {
        return intervaloTiempoExplicito;
    }

    /**
     * @param intervaloTiempoExplicito the intervaloTiempoExplicito to set
     */
    public void setIntervaloTiempoExplicito(String intervaloTiempoExplicito) {
        this.intervaloTiempoExplicito = intervaloTiempoExplicito;
    }

    /**
     * @return the duracionTratamiento
     */
    public Integer getDuracionTratamiento() {
        return duracionTratamiento;
    }

    /**
     * @param duracionTratamiento the duracionTratamiento to set
     */
    public void setDuracionTratamiento(Integer duracionTratamiento) {
        this.duracionTratamiento = duracionTratamiento;
    }

    /**
     * @return the fechaIniPrescripcion
     */
    public String getFechaIniPrescripcion() {
        return fechaIniPrescripcion;
    }

    /**
     * @param fechaIniPrescripcion the fechaIniPrescripcion to set
     */
    public void setFechaIniPrescripcion(String fechaIniPrescripcion) {
        this.fechaIniPrescripcion = fechaIniPrescripcion;
    }

    /**
     * @return the fechaFinPrescripcion
     */
    public String getFechaFinPrescripcion() {
        return fechaFinPrescripcion;
    }

    /**
     * @param fechaFinPrescripcion the fechaFinPrescripcion to set
     */
    public void setFechaFinPrescripcion(String fechaFinPrescripcion) {
        this.fechaFinPrescripcion = fechaFinPrescripcion;
    }

    /**
     * @return the condicionesUso
     */
    public String getCondicionesUso() {
        return condicionesUso;
    }

    /**
     * @param condicionesUso the condicionesUso to set
     */
    public void setCondicionesUso(String condicionesUso) {
        this.condicionesUso = condicionesUso;
    }

    /**
     * @return the textoAdicional
     */
    public String getTextoAdicional() {
        return textoAdicional;
    }

    /**
     * @param textoAdicional the textoAdicional to set
     */
    public void setTextoAdicional(String textoAdicional) {
        this.textoAdicional = textoAdicional;
    }

    /**
     * @return the idMedicamento
     */
    public String getIdMedicamento() {
        return idMedicamento;
    }

    /**
     * @param idMedicamento the idMedicamento to set
     */
    public void setIdMedicamento(String idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    /**
     * @return the descMedicamento
     */
    public String getDescMedicamento() {
        return descMedicamento;
    }

    /**
     * @param descMedicamento the descMedicamento to set
     */
    public void setDescMedicamento(String descMedicamento) {
        this.descMedicamento = descMedicamento;
    }

    /**
     * @return the codigoMedicamento
     */
    public String getCodigoMedicamento() {
        return codigoMedicamento;
    }

    /**
     * @param codigoMedicamento the codigoMedicamento to set
     */
    public void setCodigoMedicamento(String codigoMedicamento) {
        this.codigoMedicamento = codigoMedicamento;
    }

    /**
     * @return the cantMinEntrega
     */
    public String getCantMinEntrega() {
        return cantMinEntrega;
    }

    /**
     * @param cantMinEntrega the cantMinEntrega to set
     */
    public void setCantMinEntrega(String cantMinEntrega) {
        this.cantMinEntrega = cantMinEntrega;
    }

    /**
     * @return the cantMaxEntrega
     */
    public String getCantMaxEntrega() {
        return cantMaxEntrega;
    }

    /**
     * @param cantMaxEntrega the cantMaxEntrega to set
     */
    public void setCantMaxEntrega(String cantMaxEntrega) {
        this.cantMaxEntrega = cantMaxEntrega;
    }

    /**
     * @return the dosis
     */
    public String getDosis() {
        return dosis;
    }

    /**
     * @param dosis the dosis to set
     */
    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    /**
     * @return the instrucciones
     */
    public String getInstrucciones() {
        return instrucciones;
    }

    /**
     * @param instrucciones the instrucciones to set
     */
    public void setInstrucciones(String instrucciones) {
        this.instrucciones = instrucciones;
    }

    /**
     * @return the cantidadAdministrar
     */
    public Long getCantidadAdministrar() {
        return cantidadAdministrar;
    }

    /**
     * @param cantidadAdministrar the cantidadAdministrar to set
     */
    public void setCantidadAdministrar(Long cantidadAdministrar) {
        this.cantidadAdministrar = cantidadAdministrar;
    }

    /**
     * @return the unidadMedida
     */
    public String getUnidadMedida() {
        return unidadMedida;
    }

    /**
     * @param unidadMedida the unidadMedida to set
     */
    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    /**
     * @return the cantidadTotal
     */
    public String getCantidadTotal() {
        return cantidadTotal;
    }

    /**
     * @param cantidadTotal the cantidadTotal to set
     */
    public void setCantidadTotal(String cantidadTotal) {
        this.cantidadTotal = cantidadTotal;
    }
    
    
}
