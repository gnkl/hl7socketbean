/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author jmejia
 */
public class MensajeHl7 implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String mensaje;
    private Integer status;
    private Date fecha;
    private Integer idrec;
    private Integer nummsg;

    public MensajeHl7() {
    }

    public MensajeHl7(Long id) {
        this.id = id;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MensajeHl7)) {
            return false;
        }
        MensajeHl7 other = (MensajeHl7) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gnk.hl7.bean.MensajeHl7[ id=" + getId() + " ]";
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the idrec
     */
    public Integer getIdrec() {
        return idrec;
    }

    /**
     * @param idrec the idrec to set
     */
    public void setIdrec(Integer idrec) {
        this.idrec = idrec;
    }

    /**
     * @return the nummsg
     */
    public Integer getNummsg() {
        return nummsg;
    }

    /**
     * @param nummsg the nummsg to set
     */
    public void setNummsg(Integer nummsg) {
        this.nummsg = nummsg;
    }
    
}
