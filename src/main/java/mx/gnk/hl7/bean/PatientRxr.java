/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;

/**
 *
 * @author jmejia
 */
public class PatientRxr implements Serializable{
    private String codigoViaAdmon;
    private String descripcionViaAdmon;
    private String idAdmonCodificacion;
    private String idSitioAdmonTratamiento;
    private String descripcionSitioAdmon;
    private String idSitioAdmonCodificacion;

    /**
     * @return the codigoViaAdmon
     */
    public String getCodigoViaAdmon() {
        return codigoViaAdmon;
    }

    /**
     * @param codigoViaAdmon the codigoViaAdmon to set
     */
    public void setCodigoViaAdmon(String codigoViaAdmon) {
        this.codigoViaAdmon = codigoViaAdmon;
    }

    /**
     * @return the descripcionViaAdmon
     */
    public String getDescripcionViaAdmon() {
        return descripcionViaAdmon;
    }

    /**
     * @param descripcionViaAdmon the descripcionViaAdmon to set
     */
    public void setDescripcionViaAdmon(String descripcionViaAdmon) {
        this.descripcionViaAdmon = descripcionViaAdmon;
    }

    /**
     * @return the idAdmonCodificacion
     */
    public String getIdAdmonCodificacion() {
        return idAdmonCodificacion;
    }

    /**
     * @param idAdmonCodificacion the idAdmonCodificacion to set
     */
    public void setIdAdmonCodificacion(String idAdmonCodificacion) {
        this.idAdmonCodificacion = idAdmonCodificacion;
    }

    /**
     * @return the idSitioAdmonTratamiento
     */
    public String getIdSitioAdmonTratamiento() {
        return idSitioAdmonTratamiento;
    }

    /**
     * @param idSitioAdmonTratamiento the idSitioAdmonTratamiento to set
     */
    public void setIdSitioAdmonTratamiento(String idSitioAdmonTratamiento) {
        this.idSitioAdmonTratamiento = idSitioAdmonTratamiento;
    }

    /**
     * @return the descripcionSitioAdmon
     */
    public String getDescripcionSitioAdmon() {
        return descripcionSitioAdmon;
    }

    /**
     * @param descripcionSitioAdmon the descripcionSitioAdmon to set
     */
    public void setDescripcionSitioAdmon(String descripcionSitioAdmon) {
        this.descripcionSitioAdmon = descripcionSitioAdmon;
    }

    /**
     * @return the idSitioAdmonCodificacion
     */
    public String getIdSitioAdmonCodificacion() {
        return idSitioAdmonCodificacion;
    }

    /**
     * @param idSitioAdmonCodificacion the idSitioAdmonCodificacion to set
     */
    public void setIdSitioAdmonCodificacion(String idSitioAdmonCodificacion) {
        this.idSitioAdmonCodificacion = idSitioAdmonCodificacion;
    }
    
}
