/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;

/**
 *
 * @author jmejia
 */
public class PatientTq1 implements Serializable{
    private String idTq1;

    /**
     * @return the idTq1
     */
    public String getIdTq1() {
        return idTq1;
    }

    /**
     * @param idTq1 the idTq1 to set
     */
    public void setIdTq1(String idTq1) {
        this.idTq1 = idTq1;
    }
}
