/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.hl7.bean;

import java.io.Serializable;

/**
 *
 * @author jmejia
 */
public class PatientRxc implements Serializable{
    private String rxComponentType;
    private String idComponente;
    private String descripcionComponente;
    private String idSistemaCodificacion;
    private String cantidadComprobante;
    private String idUnidadMedida;
    private String descripcionUnidadMedida;

    /**
     * @return the rxComponentType
     */
    public String getRxComponentType() {
        return rxComponentType;
    }

    /**
     * @param rxComponentType the rxComponentType to set
     */
    public void setRxComponentType(String rxComponentType) {
        this.rxComponentType = rxComponentType;
    }

    /**
     * @return the idComponente
     */
    public String getIdComponente() {
        return idComponente;
    }

    /**
     * @param idComponente the idComponente to set
     */
    public void setIdComponente(String idComponente) {
        this.idComponente = idComponente;
    }

    /**
     * @return the descripcionComponente
     */
    public String getDescripcionComponente() {
        return descripcionComponente;
    }

    /**
     * @param descripcionComponente the descripcionComponente to set
     */
    public void setDescripcionComponente(String descripcionComponente) {
        this.descripcionComponente = descripcionComponente;
    }

    /**
     * @return the idSistemaCodificacion
     */
    public String getIdSistemaCodificacion() {
        return idSistemaCodificacion;
    }

    /**
     * @param idSistemaCodificacion the idSistemaCodificacion to set
     */
    public void setIdSistemaCodificacion(String idSistemaCodificacion) {
        this.idSistemaCodificacion = idSistemaCodificacion;
    }

    /**
     * @return the cantidadComprobante
     */
    public String getCantidadComprobante() {
        return cantidadComprobante;
    }

    /**
     * @param cantidadComprobante the cantidadComprobante to set
     */
    public void setCantidadComprobante(String cantidadComprobante) {
        this.cantidadComprobante = cantidadComprobante;
    }

    /**
     * @return the idUnidadMedida
     */
    public String getIdUnidadMedida() {
        return idUnidadMedida;
    }

    /**
     * @param idUnidadMedida the idUnidadMedida to set
     */
    public void setIdUnidadMedida(String idUnidadMedida) {
        this.idUnidadMedida = idUnidadMedida;
    }

    /**
     * @return the descripcionUnidadMedida
     */
    public String getDescripcionUnidadMedida() {
        return descripcionUnidadMedida;
    }

    /**
     * @param descripcionUnidadMedida the descripcionUnidadMedida to set
     */
    public void setDescripcionUnidadMedida(String descripcionUnidadMedida) {
        this.descripcionUnidadMedida = descripcionUnidadMedida;
    }
}
